package starter.pages;

import net.thucydides.core.annotations.Step;

public class HomePage {

    OrangeHRMHomePage OrangeHome;

    @Step
    public void openTheApplication(){

        OrangeHome.open();

    }

    @Step
    public void enterUsername(){

        OrangeHome.enterUsername();


    }

    @Step
    public void enterPassword(){
        OrangeHome.enterPassword();

    }

    @Step
    public void clickToLogin(){
        OrangeHome.clickToLogin();

    }


}
