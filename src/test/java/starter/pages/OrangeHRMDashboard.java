package starter.pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;
import org.jruby.RubyProcess;
import org.junit.Assert;

public class OrangeHRMDashboard extends PageObject {


    public void verifyLogin(){

       String Current_url = getDriver().getCurrentUrl();
       System.out.println(Current_url);
       Assert.assertEquals(Current_url,"https://opensource-demo.orangehrmlive.com/index.php/dashboard");

   }

}
