package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.pages.DashboardPage;
import starter.pages.HomePage;

public class LoginStepDefinitions {
    @Steps
    HomePage home;
    @Steps
    DashboardPage dash;

    @Given("the user opens the application")
    public void the_user_opens_the_application() {
        home.openTheApplication();

    }

    @When("the user enters username")
    public void the_user_enters_username() {
        home.enterUsername();
    }

    @When("the user enters password")
    public void the_user_enters_password() {
        home.enterPassword();

    }

    @When("the user clicks to login")
    public void the_user_clicks_to_login() {
        home.clickToLogin();
    }

    @Then("the user will be redirected to the dashboard")
    public void the_user_will_be_redirected_to_the_dashboard()
    {
        dash.verifyLogin();
    }


}
