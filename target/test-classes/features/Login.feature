Feature: Login to HRM Orange
  Scenario: Login with Admin
    Given the user opens the application
    When the user enters username
    And the user enters password
    And the user clicks to login
    Then the user will be redirected to the dashboard

#@Manual
#  Scenario: Login with Admin1
#    Given the user opens the application1
#    When the user enters username1
#    And the user enters password1
#    And the user clicks to login1
#    Then the user will be redirected to the dashboard1
#
#@issue:TAN-898
#@Manual
#  Scenario: Login with Admin2
#    Given the user opens the application2
#    When the user enters username2
#    And the user enters password2
#    And the user clicks to login2
#    Then the user will be redirected to the dashboard2
#
#@Manual
#@Manual-result:passed
#  Scenario: Login with Admin3
#    Given the user opens the application3
#    When the user enters username3
#    And the user enters password3
#    And the user clicks to login3
#    Then the user will be redirected to the dashboard3
#
#@Manual
#@Manual-result:failed
#  Scenario: Login with Admin4
#    Given the user opens the application4
#    When the user enters username4
#    And the user enters password4
#    And the user clicks to login4
#    Then the user will be redirected to the dashboard4
#
#@Manual
#@Manual-result:compromised
#  Scenario: Login with Admin5
#    Given the user opens the application5
#    When the user enters username5
#    And the user enters password5
#    And the user clicks to login5
#    Then the user will be redirected to the dashboard5
#
